﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace webxlparser.Controllers
{
	

    public class NodeController : ApiController
    {
		static Node[] CreateTestData()
		{
			Node Node1 = new Node { Name = "Foo" };
			Node Node2 = new Node { Name = "Bar" };

			Node[] Nodes = new Node[]
			{
				Node1,
				Node2
			};

			Node2.Children.AddLast(Node1);

			return Nodes;
		}

        public IEnumerable<Node> GetAllNodes()
		{
			return CreateTestData();
		}

		public IHttpActionResult GetNode(string name)
		{
			Node[] Nodes = CreateTestData();
			var Node = Nodes.FirstOrDefault((p) => p.Name == name);
			if (Node == null)
			{
				return NotFound();
			}
			return Ok(Node);
		}
    }
}
