﻿using System;
using System.Collections.Generic;

namespace webxlparser
{
	public class Node
	{
		public LinkedList<Node> Children = new LinkedList<Node> { };
		public string Name { get; set; }

	}
}
